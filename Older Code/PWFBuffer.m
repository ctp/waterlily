//
//  PWFBuffer.m
//  Networker
//
//  Created by Chris Parker on 5/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PWFBuffer.h"


@implementation PWFBuffer

#pragma mark Initalization and Memory Management

- (id)init {
    if ((self = [super init])) {
        _buffer = [[NSMutableData alloc] initWithCapacity:1024];
    }
    
    return self;
}

- (void)dealloc {
    [_buffer release];
    [super dealloc];
}

#pragma mark -

- (void)addData:(NSData *)data {
    [_buffer appendData:data];
}

- (NSData *)dataUpToByte:(uint8_t)byte {
    NSData *newData = nil;
    uint8_t *pointer = (uint8_t *)[_buffer bytes];
    uint8_t *bufferStart = (uint8_t *)[_buffer bytes];
    NSUInteger bufferLength = [_buffer length];

    while ((*pointer != byte) && (pointer < bufferStart + bufferLength)) pointer++;
        
    if (*pointer == byte && (pointer - bufferStart < [_buffer length])) {
        pointer++;
        NSRange dataRange = NSMakeRange(0, pointer - bufferStart);
        newData = [_buffer subdataWithRange:dataRange];
        [_buffer replaceBytesInRange:dataRange withBytes:NULL length:0];
    }
    
    return newData;
}

@end
