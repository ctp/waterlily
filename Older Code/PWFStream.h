/*  WLStream.h - Waterlily client project
    Copyright 2009, Chris Parker <ctp@mac.com>. All rights reserved.
 */

#import <Foundation/NSObject.h>
#import <dispatch/dispatch.h>

@class NSHost, NSInputStream, NSOutputStream;

@interface PWFStream : NSObject {
  @package
    NSInputStream *_inputStream;
    NSOutputStream *_outputStream;
    NSMutableArray *_outputDataQueue;
    dispatch_queue_t _readingQueue;
    dispatch_queue_t _writingQueue;
    dispatch_source_t _readingSource;
    dispatch_source_t _writingSource;
}

- (id)initWithHost:(NSHost *)host port:(int32_t)port useSSL:(BOOL)useSSL;

@property (readonly, getter=isConnected) BOOL connected;
@property (readwrite, copy) void (^dataAvailableHandler)(NSData *);
@property (readwrite, copy) void (^errorHandler)(NSError *);
@property (readwrite, copy) void (^endHandler)(void);

- (void)open;
- (void)close;
- (void)writeData:(NSData *)data;

@end
