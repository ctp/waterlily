//
//  PWFBuffer.h
//  Networker
//
//  Created by Chris Parker on 5/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface PWFBuffer : NSObject {
  @private
    NSMutableData *_buffer;
}

- (void)addData:(NSData *)data;
- (NSData *)dataUpToByte:(uint8_t)byte;

@end
