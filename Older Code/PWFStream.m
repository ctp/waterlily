/*  WLStream.m - Waterlily client project
    Copyright 2009, Chris Parker <ctp@mac.com>. All rights reserved.
 */

/*  The experiment here is to develop an NSStream-based API which uses libdispatch under the covers to govern how the streams are serviced. Normally, you create a stream pair and have to schedule then and set delegates and implement the delegate methods and it's more frustration than anything else. Instead, I'm going to try using libdispatch on the underlying file descriptor of the stream (which should be the same for the read and write streams, although to be completely safe I should make them separate).
 */

#import "PWFStream.h"

@interface PWFStream ()

@property (readwrite, getter=isConnected) BOOL connected;

- (int)_nativeFileDescriptor;

@end

@implementation PWFStream

@synthesize connected;
@synthesize dataAvailableHandler;
@synthesize errorHandler;
@synthesize endHandler;

#pragma mark Initialization and Memory Management

- (id)initWithHost:(NSHost *)host port:(int32_t)port useSSL:(BOOL)useSSL {
    if ((self = [super init])) {
        [NSStream getStreamsToHost:host port:port inputStream:&_inputStream outputStream:&_outputStream];
        if (_inputStream && _outputStream) {
            [_inputStream retain];
            [_outputStream retain];
            if (useSSL) {
                [_inputStream setProperty:NSStreamSocketSecurityLevelKey forKey:NSStreamSocketSecurityLevelNegotiatedSSL];
                [_outputStream setProperty:NSStreamSocketSecurityLevelKey forKey:NSStreamSocketSecurityLevelNegotiatedSSL];
            }
            _outputDataQueue = [[NSMutableArray alloc] initWithCapacity:10];
        }        
    }
    
    return self;
}

- (void)finalize {
    // libdispatch doesn't support garbage collection, so we still have to manage these ourselves.
    if (_readingSource) dispatch_release(_readingSource);
    if (_writingSource) dispatch_release(_writingSource);
    if (_writingQueue) dispatch_release(_writingQueue);
        
    [super finalize];
}

- (void)dealloc {
    [_inputStream release];
    [_outputStream release];
    [_outputDataQueue release];
    
    // It's Bad Form(tm) to use accessors in -init or -dealloc methods (who knows what your subclass clients have done?) so we Block_release these directly here.
    if (dataAvailableHandler) Block_release(dataAvailableHandler);
    if (errorHandler) Block_release(errorHandler);
    if (endHandler) Block_release(endHandler);
    
    // The reading queue is one of the global concurrent queues, so we need do no memory management for it.
    if (_writingQueue) dispatch_release(_writingQueue);
    if (_readingSource) dispatch_release(_readingSource);
    if (_writingSource) dispatch_release(_writingSource);
    
    [super dealloc];
}

#pragma mark SPI

- (int)_nativeFileDescriptor {    
    NSData *nativeHandleWrapper = [NSMakeCollectable(CFReadStreamCopyProperty((CFReadStreamRef)_inputStream, kCFStreamPropertySocketNativeHandle)) autorelease];

    return nativeHandleWrapper ? *(CFSocketNativeHandle *)[nativeHandleWrapper bytes] : -1;
}

#pragma mark API

- (void)open {
    [_inputStream open];
    [_outputStream open];
    
    while (NSStreamStatusOpening == [_inputStream streamStatus]);

    if (NSStreamStatusOpen != [_inputStream streamStatus]) {
        if (errorHandler) {
            errorHandler([_inputStream streamError]);
        }
        return;
    }
    
    self.connected = YES;
    
    _readingQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _writingQueue = dispatch_queue_create("WLStream writing queue", NULL);
    _readingSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, [self _nativeFileDescriptor], 0, _readingQueue);
    _writingSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_WRITE, [self _nativeFileDescriptor], 0, _writingQueue);
    
    dispatch_source_set_event_handler(_readingSource, ^{
        if (!dispatch_source_testcancel(_readingSource)) {
            size_t estimatedBytesToRead = dispatch_source_get_data(_readingSource);
            uint8_t *buffer = (uint8_t *)malloc(estimatedBytesToRead);
            
            NSInteger bytesActuallyRead = [_inputStream read:buffer maxLength:estimatedBytesToRead];
            
            if (0 < bytesActuallyRead) {
                NSData *data = [[NSData alloc] initWithBytesNoCopy:buffer length:bytesActuallyRead freeWhenDone:YES];
                dataAvailableHandler(data);
                [data release];
            } else if (0 == bytesActuallyRead) {
                free(buffer);
                if (endHandler) {
                    endHandler();
                }
            } else {
                free(buffer);
                if (errorHandler) {
                    errorHandler([_inputStream streamError]);
                }
            }
        }
    });
    dispatch_source_set_cancel_handler(_readingSource, ^{
        [_inputStream close];
    });
    
    dispatch_source_set_event_handler(_writingSource, ^{
        if (!dispatch_source_testcancel(_writingSource)) {
            int spaceIsAvailable = dispatch_source_get_data(_writingSource);
            if ((0 < spaceIsAvailable) && (0 < [_outputDataQueue count])) {
                NSData *topmostData = [[_outputDataQueue objectAtIndex:0] retain];
                [_outputDataQueue removeObjectAtIndex:0];
                NSInteger amountWritten = [_outputStream write:[topmostData bytes] maxLength:[topmostData length]];
                
                if ((0 < amountWritten) && (amountWritten < [topmostData length])) {
                    NSData *remainingData = [[NSData alloc] initWithBytes:[topmostData bytes] + amountWritten length:[topmostData length] - amountWritten];
                    [_outputDataQueue insertObject:remainingData atIndex:0];
                    [remainingData release];
                } else if (0 == amountWritten) {
                    if (endHandler) {
                        endHandler();
                    }
                } else if (amountWritten < 0) {
                    if (errorHandler) {
                        errorHandler([_outputStream streamError]);
                    }
                }
                
                [topmostData release];
                
                if (0 == [_outputDataQueue count]) {
                    dispatch_suspend(_writingSource);
                }
            }
        }
    });
    dispatch_source_set_cancel_handler(_writingSource, ^{
        [_outputStream close];
    });
        
    dispatch_resume(_readingSource);
}

- (void)close {
    if (self.connected) {
        self.connected = NO;
        dispatch_source_cancel(_readingSource);
        dispatch_source_cancel(_writingSource);
        if (0 == [_outputDataQueue count]) dispatch_resume(_writingSource);
    }
}

- (void)writeData:(NSData *)data {
    if (self.connected) {
        dispatch_async(_writingQueue, ^{
            [_outputDataQueue addObject:data];
            
            if (1 == [_outputDataQueue count]) {
                dispatch_resume(_writingSource);
            }        
        });
    } else {
        NSLog(@"<%@ %p>: Attempt to write when stream is not open.", NSStringFromClass([self class]), self);
    }
}

@end
