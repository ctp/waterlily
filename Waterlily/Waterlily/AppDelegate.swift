//
//  AppDelegate.swift
//  Waterlily
//
//  Created by Chris Parker on 12/17/18.
//  Copyright © 2018 Playswithfire. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

